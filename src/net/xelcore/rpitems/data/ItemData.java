package net.xelcore.rpitems.data;

import net.xelcore.rpcore.data.MySQLData;
import net.xelcore.rpitems.main.Main;
import net.xelcore.rpitems.object.ItemCreator;
import net.xelcore.rpitems.object.ItemUsage;
import net.xelcore.rpitems.object.Items;
import org.bukkit.Material;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemData {

    public static void init() {
        Main.log.info(" ");
        Main.log.info("----------[Mobloader]----------");
        Main.log.info("Loading mobs...");
        loadItems();
        Main.log.info("Loaded " + ItemCreator.items.size() + " items");
        Main.log.info("----------[Mobloader]----------");
        Main.log.info(" ");
    }

    private static void loadItems() {
        try {
            final ResultSet rs = MySQLData.get("items", null);
            while (rs.next()) {
                new ItemCreator(Items.valueOf(rs.getString("item")),
                        Material.valueOf(rs.getString("stack")),
                        rs.getString("name"),
                        rs.getString("lore"),
                        rs.getBoolean("glowing"),
                        rs.getInt("level"),
                        ItemUsage.valueOf(rs.getString("required")),
                        rs.getInt("modeldata"));
                Main.log.info("Loaded item " + rs.getString("item"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
