package net.xelcore.rpitems.items;

import net.xelcore.rpitems.main.Main;
import net.xelcore.rpitems.object.ItemBuilder;
import net.xelcore.rpitems.object.ItemCreator;
import net.xelcore.rpitems.object.ItemSummoner;
import net.xelcore.rpitems.object.Items;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.ShapedRecipe;

public class Crystal {

    private final Items id = Items.CRYSTAL;

    public Crystal() {
        ItemBuilder builder = new ItemBuilder(ItemCreator.getItemData(id)) {
            @Override
            public void init() {
                useTimer(1000);
            }

            @Override
            public void run(PlayerInventory inventory, Player p) {

            }

            @Override
            public void crafting(ItemStack stack) {
                ShapedRecipe sr = new ShapedRecipe(new NamespacedKey(Main.instance, id.toString()), stack);
                sr.shape(" X ", "X X", " X ");
                sr.setIngredient('X', new RecipeChoice.ExactChoice(stack));
                addRecipe(sr);
            }

            @Override
            public void interact(PlayerInteractEvent e) {

            }

            @Override
            public void drop(PlayerDropItemEvent e) {

            }

            @Override
            public void pickup(PlayerPickupItemEvent e) {

            }

            @Override
            public void merge(ItemMergeEvent e) {

            }

            @Override
            public void despawn(ItemDespawnEvent e) {

            }
        };
        ItemSummoner.items.put(this.id, builder.getItem());
    }

}
