package net.xelcore.rpitems.object;

import net.xelcore.rpitems.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.*;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.*;

public abstract class ItemBuilder implements Listener {

    public static Plugin instance;

    public static void setPlugin(Plugin arg0) {
        instance = arg0;
    }

    private void registerEvents() {
        if (instance != null) {
            Bukkit.getPluginManager().registerEvents(this, instance);
        } else {
            throw new NullPointerException("Instance has not been set");
        }
    }

    private ItemStack item;
    private ItemMeta meta;
    private Integer level;
    private ItemUsage usage;

    private boolean useTimer;
    private Timer timer;
    private Long interval;

    public static HashMap<Player, Items> contained = new HashMap<>();

    public ItemBuilder(ItemCreator data) {
        registerEvents();
        this.item = new ItemStack(data.getMaterial());
        this.meta = this.item.getItemMeta();
        this.meta.setDisplayName(data.getName());
        this.meta.setLore(Arrays.asList(data.getLore().split(".;")));

        if (data.getModelData() != 0) {
            this.meta.setCustomModelData(data.getModelData());
        }

        if (data.isGlowing()) {
            this.meta.addEnchant(Enchantment.LURE, 1, true);
            this.meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }

        init();

        this.item.setItemMeta(this.meta);
        crafting(this.item);

        if (this.useTimer) {
            this.timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    for (Player all : Bukkit.getOnlinePlayers()) {
                        if (!all.isDead()) {
                            for (ItemStack i : all.getInventory().getContents()) {
                                if (i != null) {
                                    if (i.getItemMeta().getDisplayName().equals(item.getItemMeta().getDisplayName())) {
                                        ItemBuilder.this.run(all.getInventory(), all);
                                    }
                                }
                            }
                        }
                    }
                    if (Main.disable) {
                        this.cancel();
                    }
                }
            }, 100, interval);
        }


    }

    public abstract void init();

    public abstract void run(PlayerInventory inventory, Player player);

    public boolean inMainHand(Player p) {
        return p.getInventory().getItemInMainHand().getType() != Material.AIR && p.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(this.getName());
    }

    public boolean inOffHand(Player p) {
        return p.getInventory().getItemInOffHand().getType() != Material.AIR && p.getInventory().getItemInOffHand().getItemMeta().getDisplayName().equals(this.getName());
    }

    public void useTimer(long interval) {
        useTimer = true;
        this.interval = interval;
    }

    public void addRecipe(Recipe recipe) {
        Bukkit.getServer().addRecipe(recipe);
    }

    public abstract void crafting(ItemStack stack);

    public abstract void interact(PlayerInteractEvent e);

    public abstract void drop(PlayerDropItemEvent e);

    public abstract void pickup(PlayerPickupItemEvent e);

    public abstract void merge(ItemMergeEvent e);

    public abstract void despawn(ItemDespawnEvent e);

    @EventHandler
    public final void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getItem() != null) {
            if (e.getItem().getItemMeta().getDisplayName().equals(getItem().getItemMeta().getDisplayName())) {
                interact(e);
            }
        }
    }

    @EventHandler
    public final void onPlayerDrop(PlayerDropItemEvent e) {
        if (e.getItemDrop().getItemStack().getItemMeta().getDisplayName().equals(getItem().getItemMeta().getDisplayName())) {
            drop(e);
        }
    }

    @EventHandler
    public final void onPlayerPickup(PlayerPickupItemEvent e) {
        if (e.getItem().getItemStack().getItemMeta().getDisplayName().equals(getItem().getItemMeta().getDisplayName())) {
            pickup(e);
        }
    }

    @EventHandler
    public final void onItemMerge(ItemMergeEvent e) {
        if (e.getEntity().getItemStack().getItemMeta().getDisplayName().equals(getItem().getItemMeta().getDisplayName())) {
            merge(e);
        }
    }

    @EventHandler
    public final void onItemDespawn(ItemDespawnEvent e) {
        if (e.getEntity().getItemStack().getItemMeta().getDisplayName().equals(getItem().getItemMeta().getDisplayName())) {
            despawn(e);
        }
    }

    public ItemStack getItem() {
        return this.item;
    }

    public String getName() {
        return this.item.getItemMeta().getDisplayName();
    }


}
