package net.xelcore.rpitems.object;

import org.bukkit.Material;
import java.util.HashMap;

public class ItemCreator {

    private Items item;
    private Material material;
    private String name;
    private String lore;
    private boolean glowing;
    private Integer level;
    private ItemUsage usage;
    private Integer modeldata;

    public static HashMap<Items, ItemCreator> items = new HashMap<>();

    public static ItemCreator getItemData(Items item) {
        return items.get(item);
    }

    public ItemCreator(Items item, Material material, String name, String lore, boolean glowing, Integer level, ItemUsage usage, Integer modeldata) {
        this.item = item;
        this.material = material;
        this.name = name;
        this.lore = lore;
        this.glowing = glowing;
        this.level = level;
        this.usage = usage;
        this.modeldata = modeldata;
        items.put(this.item, this);
    }

    public Items getItem() {
        return item;
    }

    public Material getMaterial() {
        return material;
    }

    public String getName() {
        return name;
    }

    public String getLore() {
        return lore;
    }

    public boolean isGlowing() {
        return glowing;
    }

    public Integer getLevel() {
        return level;
    }

    public ItemUsage getUsage() {
        return usage;
    }

    public Integer getModelData() { return modeldata; }

}
