package net.xelcore.rpitems.command;

import net.xelcore.rpitems.object.ItemSummoner;
import net.xelcore.rpitems.object.Items;
import net.xelcore.rplib.message.ClickableMessage;
import net.xelcore.rplib.message.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ItemCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(p.hasPermission("rp.items.give")) {
                if(args.length == 1 || args.length == 2) {
                    if(args[0].equals("list")) {
                        new Message(p).prefix("Verfügbare Items");
                        for(Items i : Items.values()) {
                            new ClickableMessage(p, "§7" + i.toString().toLowerCase() + " §8- ", "§aGIVE", "item " + i.toString().toLowerCase());
                        }
                    } else {
                        boolean contains = false;
                        Items item = null;
                        for(Items i : Items.values()) {
                            if(i.toString().toLowerCase().equals(args[0].toLowerCase())) {
                                contains = true;
                                item = i;
                            }
                        }
                        if(contains) {
                            if(args.length == 2 && args[1] != null && Integer.valueOf(args[1]) != null) {
                                for(int i = 0; i < Integer.parseInt(args[1]); i++) {
                                    p.getInventory().addItem(ItemSummoner.getItem(item));
                                }
                                new Message(p).prefix("Du hast das Item " + item.toString().toLowerCase() + " " + Integer.valueOf(args[1]) + "x erhalten");
                            } else {
                                p.getInventory().addItem(ItemSummoner.getItem(item));
                                new Message(p).prefix("Du hast das Item " + item.toString().toLowerCase() + " erhalten");
                            }

                        } else {
                            new Message(p).errorprefix("Dieses Item existiert nicht, tippe §6/item list §7, um eine Liste an Items zu sehen");
                        }
                    }
                } else {
                    new Message(p).wrongcmdusage("/item <ItemID/list> [Anzahl]");
                }
            }
        } else {
            sender.sendMessage("§4Only players can execute this command");
        }
        return true;
    }
}
