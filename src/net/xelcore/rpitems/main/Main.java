package net.xelcore.rpitems.main;

import de.slikey.effectlib.EffectManager;
import net.xelcore.rpitems.command.ItemCommand;
import net.xelcore.rpitems.data.ItemData;
import net.xelcore.rpitems.object.ItemBuilder;
import net.xelcore.rplib.logger.Logger;
import org.bukkit.plugin.java.JavaPlugin;

import net.xelcore.rpitems.items.*;

import java.util.Timer;

public class Main extends JavaPlugin {

    public static Logger log;
    public static Main instance;
    public static EffectManager effectManager;
    public static boolean disable = false;

    @Override
    public void onEnable() {
        instance = this;
        log = net.xelcore.rplib.main.Main.log;
        effectManager = new EffectManager(this);

        ItemBuilder.setPlugin(this);

        ItemData.init();

        this.getCommand("item").setExecutor(new ItemCommand());

        /* Item inits */
        new Crystal();

    }

    @Override
    public void onDisable() {
        effectManager.dispose();
        disable = true;
    }
}
